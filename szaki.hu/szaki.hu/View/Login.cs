﻿using CryptSharp;
using ConnectToMysqlDatabase;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace szaki.hu
{
    public partial class Login : MaterialForm
    {
        private PictureBox bg;
        private MySQLDatabaseInterface mdi;
        private DataTable userDT;
        private bool externalServer;

        public Login()
        {
            InitializeComponent();

            this.MinimumSize = new Size(800, 450);

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

            EmailBox.Hide();
            PasswordBox.Hide();
            LoginBtn.Hide();

            //Background image
            bg = new PictureBox();
            bg.Size = new Size(100, 300);
            bg.ImageLocation = "bg.jpg";
            bg.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left);
            bg.Size = panel.Size;
            bg.SizeMode = PictureBoxSizeMode.StretchImage;

            bg.Load();
            panel.Controls.Add(bg);

            Connection.SelectedIndex = 0;
            DatabaseText.Font = materialSkinManager.ROBOTO_MEDIUM_10;
            // 
            // WelcomeText
            // 
            WelcomeText.AutoSize = true;
            WelcomeText.BackColor = System.Drawing.Color.Transparent;
            WelcomeText.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            WelcomeText.Font = new System.Drawing.Font("Arial Unicode MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            WelcomeText.Location = new System.Drawing.Point(262, 300);
            WelcomeText.Name = "WelcomeText";
            WelcomeText.Size = new System.Drawing.Size(276, 21);
            WelcomeText.Anchor = AnchorStyles.None;
            WelcomeText.TabIndex = 2;
            WelcomeText.Text = "Nyomj meg egy gombot a belépéshez";
            bg.Controls.Add(WelcomeText);

        }
        /// <summary>
        /// Placeholder eltávolítás(email)
        /// </summary>
        public void RemoveTextEmail(object sender, EventArgs e)
        {
            if (EmailBox.Text == "Email")
                EmailBox.Text = "";
        }
        /// <summary>
        /// Placeholder hozzáadása(email)
        /// </summary>
        public void AddTextEmail(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(EmailBox.Text))
                EmailBox.Text = "Email";
        }
        /// <summary>
        /// Placeholder eltávolítás(jelszó)
        /// </summary>
        public void RemoveTextPass(object sender, EventArgs e)
        {
            if (PasswordBox.Text == "Jelszó")
            {
                PasswordBox.Text = "";
                PasswordBox.UseSystemPasswordChar = true;

            }
                
        }
        /// <summary>
        /// Placeholder hozzáadása(jelszó)
        /// </summary>
        public void AddTextPass(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(PasswordBox.Text)) {
                PasswordBox.Text = "Jelszó";
                PasswordBox.UseSystemPasswordChar = false;
            } 
                
        }

        /// <summary>
        /// Gomb nyomásra a háttér eltünik
        /// </summary>
        private void Login_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (bg.Visible)
            {
                for (int i = 0; i < bg.Size.Height + 10; i++)
                {
                    bg.Location = new Point(bg.Location.X, bg.Location.Y - 1);
                }
                bg.Hide();
                EmailBox.Show();
                PasswordBox.Show();
                LoginBtn.Show();
                Connection.Show();
                DatabaseText.Show();
            }
        }

        /// <summary>
        /// Felhasználó belép a belépés gombbal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginBtn_Click(object sender, EventArgs e)
        {
            Database a = new Database(externalServer);
            mdi = a.Connect();
            mdi.open();
            string Email = EmailBox.Text;
            string Password = PasswordBox.Text;
            if (Email == "Email" || Password == "Jelszó")
            {
                MessageBox.Show("A mezőket ki kell tölteni");
            }
            else
            {
                userDT = mdi.getToDataTable("SELECT email,password FROM users WHERE email = '" + Email + "'");
                if (userDT.Rows.Count != 0)
                {

                    foreach (DataRow row in userDT.Rows)
                    {
                        if (Crypter.CheckPassword(Password, row["password"].ToString()))
                        {
                            this.Hide();
                            loggedin lg = new loggedin(externalServer);
                            lg.ShowDialog();
                            this.Close();

                        }
                        else
                        {
                            MessageBox.Show("Helytelen Jelszó!");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Helytelen E-mail cím!");
                }
            }
            mdi.close();
        }

        private void KeyPressEnter(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                LoginBtn.PerformClick();
            }

        }

        private void Connection_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.externalServer = Convert.ToBoolean(Connection.SelectedIndex);
        }
    }
}
