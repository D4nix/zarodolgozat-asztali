﻿namespace szaki.hu
{
    partial class loggedin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.TabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.TabControl = new MaterialSkin.Controls.MaterialTabControl();
            this.MainTab = new System.Windows.Forms.TabPage();
            this.panel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.UsersChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.MessagesChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.VisitationChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.usertTab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.InsideTabControl = new MaterialSkin.Controls.MaterialTabControl();
            this.ListTab = new System.Windows.Forms.TabPage();
            this.UsersListView = new MaterialSkin.Controls.MaterialListView();
            this.EditTab = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.StatesComboBox = new System.Windows.Forms.ComboBox();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.JobTitlebox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.JobNamebox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EmailBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.FirstnameBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.LastnameBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BackBtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.DeleteBtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.EditBtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.TabControl.SuspendLayout();
            this.MainTab.SuspendLayout();
            this.panel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsersChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MessagesChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitationChart)).BeginInit();
            this.usertTab.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.InsideTabControl.SuspendLayout();
            this.ListTab.SuspendLayout();
            this.EditTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // TabSelector
            // 
            this.TabSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabSelector.BaseTabControl = this.TabControl;
            this.TabSelector.Depth = 0;
            this.TabSelector.Location = new System.Drawing.Point(0, 63);
            this.TabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.TabSelector.Name = "TabSelector";
            this.TabSelector.Size = new System.Drawing.Size(800, 50);
            this.TabSelector.TabIndex = 0;
            this.TabSelector.Text = "materialTabSelector1";
            // 
            // TabControl
            // 
            this.TabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl.Controls.Add(this.MainTab);
            this.TabControl.Controls.Add(this.usertTab);
            this.TabControl.Depth = 0;
            this.TabControl.Location = new System.Drawing.Point(0, 111);
            this.TabControl.MouseState = MaterialSkin.MouseState.HOVER;
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(800, 341);
            this.TabControl.TabIndex = 1;
            this.TabControl.SelectedIndexChanged += new System.EventHandler(this.TabControl_SelectedIndexChanged);
            // 
            // MainTab
            // 
            this.MainTab.Controls.Add(this.panel);
            this.MainTab.Location = new System.Drawing.Point(4, 22);
            this.MainTab.Name = "MainTab";
            this.MainTab.Padding = new System.Windows.Forms.Padding(3);
            this.MainTab.Size = new System.Drawing.Size(792, 315);
            this.MainTab.TabIndex = 0;
            this.MainTab.Text = "Főoldal";
            this.MainTab.UseVisualStyleBackColor = true;
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.AutoScroll = true;
            this.panel.Controls.Add(this.tableLayoutPanel1);
            this.panel.Controls.Add(this.VisitationChart);
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(792, 315);
            this.panel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.UsersChart, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.MessagesChart, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 321);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 237F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(775, 237);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // UsersChart
            // 
            chartArea1.Name = "ChartArea1";
            this.UsersChart.ChartAreas.Add(chartArea1);
            this.UsersChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.UsersChart.Legends.Add(legend1);
            this.UsersChart.Location = new System.Drawing.Point(3, 3);
            this.UsersChart.Name = "UsersChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.UsersChart.Series.Add(series1);
            this.UsersChart.Size = new System.Drawing.Size(381, 231);
            this.UsersChart.TabIndex = 1;
            this.UsersChart.Text = "chart1";
            // 
            // MessagesChart
            // 
            chartArea2.Name = "ChartArea1";
            this.MessagesChart.ChartAreas.Add(chartArea2);
            this.MessagesChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.MessagesChart.Legends.Add(legend2);
            this.MessagesChart.Location = new System.Drawing.Point(390, 3);
            this.MessagesChart.Name = "MessagesChart";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series2.Legend = "Legend1";
            series2.Name = "Üzenetek";
            series2.YValuesPerPoint = 2;
            this.MessagesChart.Series.Add(series2);
            this.MessagesChart.Size = new System.Drawing.Size(382, 231);
            this.MessagesChart.TabIndex = 2;
            this.MessagesChart.Text = "chart2";
            // 
            // VisitationChart
            // 
            chartArea3.AlignmentOrientation = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal;
            chartArea3.Name = "ChartArea1";
            chartArea4.AlignmentOrientation = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal;
            chartArea4.Name = "ChartArea2";
            this.VisitationChart.ChartAreas.Add(chartArea3);
            this.VisitationChart.ChartAreas.Add(chartArea4);
            this.VisitationChart.Dock = System.Windows.Forms.DockStyle.Top;
            legend3.Name = "Legend1";
            this.VisitationChart.Legends.Add(legend3);
            this.VisitationChart.Location = new System.Drawing.Point(0, 0);
            this.VisitationChart.Name = "VisitationChart";
            series3.ChartArea = "ChartArea1";
            series3.Color = System.Drawing.Color.LightSlateGray;
            series3.Legend = "Legend1";
            series3.Name = "Vállalkozó";
            series4.ChartArea = "ChartArea1";
            series4.Color = System.Drawing.Color.RoyalBlue;
            series4.Legend = "Legend1";
            series4.Name = "Látogatottság";
            series5.ChartArea = "ChartArea1";
            series5.Color = System.Drawing.Color.Goldenrod;
            series5.Legend = "Legend1";
            series5.Name = "Magánszemély";
            series6.ChartArea = "ChartArea2";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series6.Legend = "Legend1";
            series6.Name = "Összes belépés";
            this.VisitationChart.Series.Add(series3);
            this.VisitationChart.Series.Add(series4);
            this.VisitationChart.Series.Add(series5);
            this.VisitationChart.Series.Add(series6);
            this.VisitationChart.Size = new System.Drawing.Size(775, 315);
            this.VisitationChart.TabIndex = 0;
            this.VisitationChart.Text = "chart";
            // 
            // usertTab
            // 
            this.usertTab.Controls.Add(this.tableLayoutPanel2);
            this.usertTab.Location = new System.Drawing.Point(4, 22);
            this.usertTab.Name = "usertTab";
            this.usertTab.Padding = new System.Windows.Forms.Padding(3);
            this.usertTab.Size = new System.Drawing.Size(792, 315);
            this.usertTab.TabIndex = 1;
            this.usertTab.Text = "Felhasználók";
            this.usertTab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.44305F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.55694F));
            this.tableLayoutPanel2.Controls.Add(this.InsideTabControl, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(786, 309);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // InsideTabControl
            // 
            this.InsideTabControl.Controls.Add(this.ListTab);
            this.InsideTabControl.Controls.Add(this.EditTab);
            this.InsideTabControl.Depth = 0;
            this.InsideTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InsideTabControl.Location = new System.Drawing.Point(3, 3);
            this.InsideTabControl.MouseState = MaterialSkin.MouseState.HOVER;
            this.InsideTabControl.Name = "InsideTabControl";
            this.InsideTabControl.SelectedIndex = 0;
            this.InsideTabControl.Size = new System.Drawing.Size(594, 303);
            this.InsideTabControl.TabIndex = 3;
            this.InsideTabControl.SelectedIndexChanged += new System.EventHandler(this.TabControl_SelectedIndexChanged);
            // 
            // ListTab
            // 
            this.ListTab.Controls.Add(this.UsersListView);
            this.ListTab.Location = new System.Drawing.Point(4, 22);
            this.ListTab.Name = "ListTab";
            this.ListTab.Padding = new System.Windows.Forms.Padding(3);
            this.ListTab.Size = new System.Drawing.Size(586, 277);
            this.ListTab.TabIndex = 0;
            this.ListTab.Text = "ListTab";
            this.ListTab.UseVisualStyleBackColor = true;
            // 
            // UsersListView
            // 
            this.UsersListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UsersListView.Depth = 0;
            this.UsersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UsersListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.UsersListView.FullRowSelect = true;
            this.UsersListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.UsersListView.Location = new System.Drawing.Point(3, 3);
            this.UsersListView.MouseLocation = new System.Drawing.Point(-1, -1);
            this.UsersListView.MouseState = MaterialSkin.MouseState.OUT;
            this.UsersListView.MultiSelect = false;
            this.UsersListView.Name = "UsersListView";
            this.UsersListView.OwnerDraw = true;
            this.UsersListView.Size = new System.Drawing.Size(580, 271);
            this.UsersListView.TabIndex = 0;
            this.UsersListView.UseCompatibleStateImageBehavior = false;
            this.UsersListView.View = System.Windows.Forms.View.Details;
            // 
            // EditTab
            // 
            this.EditTab.AutoScroll = true;
            this.EditTab.BackColor = System.Drawing.Color.White;
            this.EditTab.Controls.Add(this.groupBox2);
            this.EditTab.Controls.Add(this.groupBox1);
            this.EditTab.Controls.Add(this.materialLabel1);
            this.EditTab.Location = new System.Drawing.Point(4, 22);
            this.EditTab.Name = "EditTab";
            this.EditTab.Padding = new System.Windows.Forms.Padding(3);
            this.EditTab.Size = new System.Drawing.Size(586, 277);
            this.EditTab.TabIndex = 1;
            this.EditTab.Text = "EditTab";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.StatesComboBox);
            this.groupBox2.Controls.Add(this.materialLabel7);
            this.groupBox2.Controls.Add(this.JobTitlebox);
            this.groupBox2.Controls.Add(this.materialLabel6);
            this.groupBox2.Controls.Add(this.JobNamebox);
            this.groupBox2.Controls.Add(this.materialLabel5);
            this.groupBox2.Location = new System.Drawing.Point(15, 171);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(553, 154);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vállalkozás adatai";
            this.groupBox2.Visible = false;
            // 
            // StatesComboBox
            // 
            this.StatesComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StatesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StatesComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatesComboBox.FormattingEnabled = true;
            this.StatesComboBox.Location = new System.Drawing.Point(147, 107);
            this.StatesComboBox.Name = "StatesComboBox";
            this.StatesComboBox.Size = new System.Drawing.Size(371, 21);
            this.StatesComboBox.TabIndex = 9;
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(19, 109);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(72, 19);
            this.materialLabel7.TabIndex = 8;
            this.materialLabel7.Text = "Székhely:";
            // 
            // JobTitlebox
            // 
            this.JobTitlebox.BackColor = System.Drawing.SystemColors.Control;
            this.JobTitlebox.Depth = 0;
            this.JobTitlebox.Hint = "";
            this.JobTitlebox.Location = new System.Drawing.Point(147, 68);
            this.JobTitlebox.MaxLength = 32767;
            this.JobTitlebox.MouseState = MaterialSkin.MouseState.HOVER;
            this.JobTitlebox.Name = "JobTitlebox";
            this.JobTitlebox.PasswordChar = '\0';
            this.JobTitlebox.SelectedText = "";
            this.JobTitlebox.SelectionLength = 0;
            this.JobTitlebox.SelectionStart = 0;
            this.JobTitlebox.Size = new System.Drawing.Size(371, 23);
            this.JobTitlebox.TabIndex = 5;
            this.JobTitlebox.TabStop = false;
            this.JobTitlebox.Text = "Szakma";
            this.JobTitlebox.UseSystemPasswordChar = false;
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(19, 72);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(66, 19);
            this.materialLabel6.TabIndex = 6;
            this.materialLabel6.Text = "Szakma:";
            // 
            // JobNamebox
            // 
            this.JobNamebox.BackColor = System.Drawing.SystemColors.Control;
            this.JobNamebox.Depth = 0;
            this.JobNamebox.Hint = "";
            this.JobNamebox.Location = new System.Drawing.Point(147, 30);
            this.JobNamebox.MaxLength = 32767;
            this.JobNamebox.MouseState = MaterialSkin.MouseState.HOVER;
            this.JobNamebox.Name = "JobNamebox";
            this.JobNamebox.PasswordChar = '\0';
            this.JobNamebox.SelectedText = "";
            this.JobNamebox.SelectionLength = 0;
            this.JobNamebox.SelectionStart = 0;
            this.JobNamebox.Size = new System.Drawing.Size(371, 23);
            this.JobNamebox.TabIndex = 4;
            this.JobNamebox.TabStop = false;
            this.JobNamebox.Text = "Válalkozás neve";
            this.JobNamebox.UseSystemPasswordChar = false;
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(19, 31);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(122, 19);
            this.materialLabel5.TabIndex = 4;
            this.materialLabel5.Text = "Válalkozás neve:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EmailBox);
            this.groupBox1.Controls.Add(this.materialLabel2);
            this.groupBox1.Controls.Add(this.FirstnameBox);
            this.groupBox1.Controls.Add(this.LastnameBox);
            this.groupBox1.Controls.Add(this.materialLabel3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupBox1.Location = new System.Drawing.Point(15, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(553, 119);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Személyes adatok";
            // 
            // EmailBox
            // 
            this.EmailBox.BackColor = System.Drawing.SystemColors.Control;
            this.EmailBox.Depth = 0;
            this.EmailBox.Hint = "";
            this.EmailBox.Location = new System.Drawing.Point(138, 57);
            this.EmailBox.MaxLength = 32767;
            this.EmailBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.EmailBox.Name = "EmailBox";
            this.EmailBox.PasswordChar = '\0';
            this.EmailBox.SelectedText = "";
            this.EmailBox.SelectionLength = 0;
            this.EmailBox.SelectionStart = 0;
            this.EmailBox.Size = new System.Drawing.Size(371, 23);
            this.EmailBox.TabIndex = 3;
            this.EmailBox.TabStop = false;
            this.EmailBox.Text = "Email";
            this.EmailBox.UseSystemPasswordChar = false;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(19, 24);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(39, 19);
            this.materialLabel2.TabIndex = 1;
            this.materialLabel2.Text = "Név:";
            // 
            // FirstnameBox
            // 
            this.FirstnameBox.BackColor = System.Drawing.SystemColors.Control;
            this.FirstnameBox.Depth = 0;
            this.FirstnameBox.Hint = "";
            this.FirstnameBox.Location = new System.Drawing.Point(138, 20);
            this.FirstnameBox.MaxLength = 32767;
            this.FirstnameBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.FirstnameBox.Name = "FirstnameBox";
            this.FirstnameBox.PasswordChar = '\0';
            this.FirstnameBox.SelectedText = "";
            this.FirstnameBox.SelectionLength = 0;
            this.FirstnameBox.SelectionStart = 0;
            this.FirstnameBox.Size = new System.Drawing.Size(163, 23);
            this.FirstnameBox.TabIndex = 1;
            this.FirstnameBox.TabStop = false;
            this.FirstnameBox.Text = "Vezetéknév";
            this.FirstnameBox.UseSystemPasswordChar = false;
            // 
            // LastnameBox
            // 
            this.LastnameBox.BackColor = System.Drawing.SystemColors.Control;
            this.LastnameBox.Depth = 0;
            this.LastnameBox.Hint = "";
            this.LastnameBox.Location = new System.Drawing.Point(345, 20);
            this.LastnameBox.MaxLength = 32767;
            this.LastnameBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.LastnameBox.Name = "LastnameBox";
            this.LastnameBox.PasswordChar = '\0';
            this.LastnameBox.SelectedText = "";
            this.LastnameBox.SelectionLength = 0;
            this.LastnameBox.SelectionStart = 0;
            this.LastnameBox.Size = new System.Drawing.Size(164, 23);
            this.LastnameBox.TabIndex = 2;
            this.LastnameBox.TabStop = false;
            this.LastnameBox.Text = "Keresztnév";
            this.LastnameBox.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(19, 57);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(51, 19);
            this.materialLabel3.TabIndex = 3;
            this.materialLabel3.Text = "Email:";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(7, 7);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(90, 19);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = "Szerkesztés";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.BackBtn);
            this.panel1.Controls.Add(this.DeleteBtn);
            this.panel1.Controls.Add(this.EditBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(603, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(180, 303);
            this.panel1.TabIndex = 0;
            // 
            // BackBtn
            // 
            this.BackBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BackBtn.AutoSize = true;
            this.BackBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackBtn.BackColor = System.Drawing.Color.White;
            this.BackBtn.Depth = 0;
            this.BackBtn.Icon = null;
            this.BackBtn.Location = new System.Drawing.Point(36, 102);
            this.BackBtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.BackBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Primary = false;
            this.BackBtn.Size = new System.Drawing.Size(68, 36);
            this.BackBtn.TabIndex = 3;
            this.BackBtn.Text = "Vissza";
            this.BackBtn.UseVisualStyleBackColor = false;
            this.BackBtn.Visible = false;
            this.BackBtn.Click += new System.EventHandler(this.BackBtn_Click);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DeleteBtn.AutoSize = true;
            this.DeleteBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DeleteBtn.BackColor = System.Drawing.Color.White;
            this.DeleteBtn.Depth = 0;
            this.DeleteBtn.Icon = null;
            this.DeleteBtn.Location = new System.Drawing.Point(36, 54);
            this.DeleteBtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DeleteBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Primary = false;
            this.DeleteBtn.Size = new System.Drawing.Size(71, 36);
            this.DeleteBtn.TabIndex = 2;
            this.DeleteBtn.Text = "Törlés";
            this.DeleteBtn.UseVisualStyleBackColor = false;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // EditBtn
            // 
            this.EditBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.EditBtn.AutoSize = true;
            this.EditBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.EditBtn.BackColor = System.Drawing.Color.White;
            this.EditBtn.Depth = 0;
            this.EditBtn.Icon = null;
            this.EditBtn.Location = new System.Drawing.Point(36, 6);
            this.EditBtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.EditBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.EditBtn.Name = "EditBtn";
            this.EditBtn.Primary = false;
            this.EditBtn.Size = new System.Drawing.Size(111, 36);
            this.EditBtn.TabIndex = 1;
            this.EditBtn.Text = "Szerkesztés";
            this.EditBtn.UseVisualStyleBackColor = false;
            this.EditBtn.Click += new System.EventHandler(this.EditBtn_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // loggedin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.TabSelector);
            this.Name = "loggedin";
            this.Text = "Szaki.hu-Admin";
            this.TabControl.ResumeLayout(false);
            this.MainTab.ResumeLayout(false);
            this.panel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsersChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MessagesChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitationChart)).EndInit();
            this.usertTab.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.InsideTabControl.ResumeLayout(false);
            this.ListTab.ResumeLayout(false);
            this.EditTab.ResumeLayout(false);
            this.EditTab.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabSelector TabSelector;
        private MaterialSkin.Controls.MaterialTabControl TabControl;
        private System.Windows.Forms.TabPage usertTab;
        private MaterialSkin.Controls.MaterialListView UsersListView;
        private System.Windows.Forms.TabPage MainTab;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart UsersChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart MessagesChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart VisitationChart;
        private MaterialSkin.Controls.MaterialFlatButton EditBtn;
        private MaterialSkin.Controls.MaterialTabControl InsideTabControl;
        private System.Windows.Forms.TabPage ListTab;
        private System.Windows.Forms.TabPage EditTab;
        private MaterialSkin.Controls.MaterialFlatButton DeleteBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField FirstnameBox;
        private MaterialSkin.Controls.MaterialSingleLineTextField LastnameBox;
        private MaterialSkin.Controls.MaterialFlatButton BackBtn;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialSingleLineTextField EmailBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialSingleLineTextField JobTitlebox;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.ComboBox StatesComboBox;
        private MaterialSkin.Controls.MaterialSingleLineTextField JobNamebox;
    }
}