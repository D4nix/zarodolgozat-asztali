﻿namespace szaki.hu
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EmailBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.PasswordBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.panel = new System.Windows.Forms.Panel();
            this.DatabaseText = new MaterialSkin.Controls.MaterialLabel();
            this.LoginBtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.Connection = new System.Windows.Forms.ComboBox();
            this.WelcomeText = new System.Windows.Forms.Label();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // EmailBox
            // 
            this.EmailBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.EmailBox.Depth = 0;
            this.EmailBox.Hint = "";
            this.EmailBox.Location = new System.Drawing.Point(270, 265);
            this.EmailBox.MaxLength = 32767;
            this.EmailBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.EmailBox.Name = "EmailBox";
            this.EmailBox.PasswordChar = '\0';
            this.EmailBox.SelectedText = "";
            this.EmailBox.SelectionLength = 0;
            this.EmailBox.SelectionStart = 0;
            this.EmailBox.Size = new System.Drawing.Size(264, 23);
            this.EmailBox.TabIndex = 0;
            this.EmailBox.TabStop = false;
            this.EmailBox.Text = "Email";
            this.EmailBox.UseSystemPasswordChar = false;
            this.EmailBox.Enter += new System.EventHandler(this.RemoveTextEmail);
            this.EmailBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEnter);
            this.EmailBox.Leave += new System.EventHandler(this.AddTextEmail);
            // 
            // PasswordBox
            // 
            this.PasswordBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PasswordBox.Depth = 0;
            this.PasswordBox.Hint = "";
            this.PasswordBox.Location = new System.Drawing.Point(270, 305);
            this.PasswordBox.MaxLength = 32767;
            this.PasswordBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.PasswordBox.Name = "PasswordBox";
            this.PasswordBox.PasswordChar = '\0';
            this.PasswordBox.SelectedText = "";
            this.PasswordBox.SelectionLength = 0;
            this.PasswordBox.SelectionStart = 0;
            this.PasswordBox.Size = new System.Drawing.Size(264, 23);
            this.PasswordBox.TabIndex = 1;
            this.PasswordBox.TabStop = false;
            this.PasswordBox.Text = "Jelszó";
            this.PasswordBox.UseSystemPasswordChar = false;
            this.PasswordBox.Enter += new System.EventHandler(this.RemoveTextPass);
            this.PasswordBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEnter);
            this.PasswordBox.Leave += new System.EventHandler(this.AddTextPass);
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.Controls.Add(this.DatabaseText);
            this.panel.Controls.Add(this.LoginBtn);
            this.panel.Controls.Add(this.EmailBox);
            this.panel.Controls.Add(this.PasswordBox);
            this.panel.Controls.Add(this.Connection);
            this.panel.Location = new System.Drawing.Point(-1, 63);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(802, 388);
            this.panel.TabIndex = 3;
            // 
            // DatabaseText
            // 
            this.DatabaseText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DatabaseText.Depth = 0;
            this.DatabaseText.Font = new System.Drawing.Font("Roboto", 11F);
            this.DatabaseText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DatabaseText.Location = new System.Drawing.Point(572, 27);
            this.DatabaseText.MouseState = MaterialSkin.MouseState.HOVER;
            this.DatabaseText.Name = "DatabaseText";
            this.DatabaseText.Size = new System.Drawing.Size(227, 71);
            this.DatabaseText.TabIndex = 11;
            this.DatabaseText.Text = "Külső adatbázis konfigurációt a \"databaseConfig\" fájlban megteheti";
            this.DatabaseText.Visible = false;
            // 
            // LoginBtn
            // 
            this.LoginBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LoginBtn.AutoSize = true;
            this.LoginBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LoginBtn.Depth = 0;
            this.LoginBtn.Icon = null;
            this.LoginBtn.Location = new System.Drawing.Point(372, 337);
            this.LoginBtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.LoginBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.LoginBtn.Name = "LoginBtn";
            this.LoginBtn.Primary = false;
            this.LoginBtn.Size = new System.Drawing.Size(77, 36);
            this.LoginBtn.TabIndex = 2;
            this.LoginBtn.Text = "Belépés";
            this.LoginBtn.UseVisualStyleBackColor = true;
            this.LoginBtn.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // Connection
            // 
            this.Connection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Connection.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Connection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Connection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Connection.FormattingEnabled = true;
            this.Connection.Items.AddRange(new object[] {
            "Localhost",
            "Külső szerver"});
            this.Connection.Location = new System.Drawing.Point(576, 3);
            this.Connection.Name = "Connection";
            this.Connection.Size = new System.Drawing.Size(213, 21);
            this.Connection.TabIndex = 10;
            this.Connection.Visible = false;
            this.Connection.SelectedIndexChanged += new System.EventHandler(this.Connection_SelectedIndexChanged);
            // 
            // WelcomeText
            // 
            this.WelcomeText.Location = new System.Drawing.Point(0, 0);
            this.WelcomeText.Name = "WelcomeText";
            this.WelcomeText.Size = new System.Drawing.Size(100, 23);
            this.WelcomeText.TabIndex = 0;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel);
            this.Name = "Login";
            this.Text = "Szaki.hu-Admin";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Login_KeyPress);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField EmailBox;
        private MaterialSkin.Controls.MaterialSingleLineTextField PasswordBox;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label WelcomeText;
        private MaterialSkin.Controls.MaterialFlatButton LoginBtn;
        private System.Windows.Forms.ComboBox Connection;
        private MaterialSkin.Controls.MaterialLabel DatabaseText;
    }
}

