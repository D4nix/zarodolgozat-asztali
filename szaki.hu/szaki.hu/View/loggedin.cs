﻿using ConnectToMysqlDatabase;
using szaki.hu.Model;
using Validation;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace szaki.hu
{
    public partial class loggedin : MaterialForm
    {
        #region Váltózok
        private MySQLDatabaseInterface mdi;
        private DataTable VisitationDT;
        private DataTable UserDT;
        private List<Contractor> Users;
        private int SelectedUserId;
        private bool externalServer;
        #endregion

        /// <summary>
        /// Loggedin form konsruktora.Beállitja az alap dolgokat
        /// </summary>
        public loggedin(bool externalServer)
        {
            this.externalServer = externalServer;
            InitializeComponent();
            LoadVisitationChart();
            LoadUsersChart();
            LoadSentMessages();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

            StatesComboBox.Font = materialSkinManager.ROBOTO_MEDIUM_11;

            this.MinimumSize = new Size(800, 450);
            UsersListView.FullRowSelect = true;

            UsersListView.Columns.Add("Id");
            UsersListView.Columns.Add("Vezetéknév");
            UsersListView.Columns.Add("Keresztnév");
            UsersListView.Columns.Add("Email");
            UsersListView.Columns.Add("Kategória");

            /*UsersListView.Columns[0].Width = -1;
            UsersListView.Columns[1].Width = -1;
            UsersListView.Columns[2].Width = -1;
            UsersListView.Columns[3].Width = -1;
            UsersListView.Columns[4].Width = -1;*/

           
        }

        /// <summary>
        /// Betölti az adatok az adatbázisból a diagramba 
        /// </summary>
        private void LoadVisitationChart() {
            Database a = new Database(externalServer);
            mdi = a.Connect();
            mdi.open();

            string sql;

            sql = "SELECT COUNT(log.userID) AS 'ContuserID',site AS 'Contsite' FROM users INNER JOIN log ON users.userID = log.userID WHERE category LIKE 'contractor' GROUP BY site";

            VisitationDT = mdi.getToDataTable(sql);

            sql = "SELECT COUNT(userID) AS userID,site FROM log GROUP BY site";

            VisitationDT.Merge(mdi.getToDataTable(sql));

            sql = "SELECT COUNT(log.userID) AS 'InduserID',site AS 'Indsite' FROM users INNER JOIN log ON users.userID = log.userID WHERE category LIKE 'individual' GROUP BY site";

            VisitationDT.Merge(mdi.getToDataTable(sql));

            sql = "SELECT COUNT(id) AS count,DATE_FORMAT(date, '%Y %M %d')  AS 'date'"
                  +" FROM log WHERE activity LIKE 'visit'"
                  +" GROUP BY year(date), month(date), day(date)";

            VisitationDT.Merge(mdi.getToDataTable(sql));

            VisitationChart.DataSource = VisitationDT;

            VisitationChart.Series[0].XValueMember = "Contsite";
            VisitationChart.Series[0].YValueMembers = "ContuserID";

            VisitationChart.Series[1].XValueMember = "site";
            VisitationChart.Series[1].YValueMembers = "userID";

            VisitationChart.Series[2].XValueMember = "Indsite";
            VisitationChart.Series[2].YValueMembers = "InduserID";

            VisitationChart.Series[3].XValueMember = "date";
            VisitationChart.Series[3].YValueMembers = "count";

            VisitationChart.DataBind();


            mdi.close();

        }

        /// <summary>
        /// Betölti az adatok az adatbázisból a diagramba 
        /// </summary>
        private void LoadUsersChart() {
            DataTable UsersDT;
            Database a = new Database(externalServer);
            mdi = a.Connect();
            mdi.open();

            string sql = "SELECT COUNT(userID) AS \"Users\" FROM users GROUP BY category";
            UsersDT = mdi.getToDataTable(sql);

            UsersChart.Series[0]["PieLabelStyle"] = "Disabled";

            UsersChart.Series[0].Points.DataBindXY(
                 new[] { "Vállalkozó", "Magánszemély" },
                 new[] { Convert.ToInt32(UsersDT.Rows[0][0].ToString()), Convert.ToInt32(UsersDT.Rows[1][0].ToString()) }
            );
            UsersChart.DataBind();
            mdi.close();
        }

        /// <summary>
        /// Betölti a felhasználókat List View-ba
        /// </summary>
        private void LoadUsersToListView() {
            Database a = new Database(externalServer);
            Users = new List<Contractor>();
            UsersListView.Items.Clear();
            mdi = a.Connect();
            mdi.open();
            UserDT = mdi.getToDataTable("SELECT users.userID,first_name,last_name,email,category,COALESCE(job_name,''), COALESCE(job_title,''),COALESCE(state_name,''),COALESCE(state,0) FROM users LEFT JOIN contractors ON users.userID = contractors.userID LEFT JOIN states ON contractors.state = states.id ORDER BY users.userID");
            
            foreach (DataRow row in UserDT.Rows)
            {
                Contractor user = new Contractor(
                    Convert.ToInt32(row[0]),
                    row[1].ToString(),
                    row[2].ToString(),
                    row[3].ToString(),
                    row[4].ToString(),
                    row[5].ToString(),
                    row[6].ToString(),
                    row[7].ToString(),
                    Convert.ToInt32(row[8])
                );
                Users.Add(user);
                ListViewItem userItem = new ListViewItem(user.id.ToString());
                userItem.SubItems.Add(user.first_name);
                userItem.SubItems.Add(user.last_name);
                userItem.SubItems.Add(user.email);
                userItem.SubItems.Add(user.category);
                UsersListView.Items.Add(userItem);
            }
            mdi.close();
        }

        /// <summary>
        /// Betölti az adatokat a diagramba
        /// </summary>
        private void LoadSentMessages() {
            DataTable MessagesDT;
            Database a = new Database(externalServer);
            mdi = a.Connect();
            mdi.open();

            string sql = "SELECT COUNT(id) AS count, month(date) as month FROM chat GROUP BY month(date)";

            MessagesDT = mdi.getToDataTable(sql);

            MessagesChart.DataSource = MessagesDT;

            MessagesChart.Series[0].XValueMember = "month";
            MessagesChart.Series[0].YValueMembers = "count";

            MessagesChart.DataBind();


            mdi.close();
        }

        private void TabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TabControl.SelectedIndex == 1) {
                LoadUsersToListView();
                UsersListView.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.ColumnContent);
                UsersListView.AutoResizeColumn(1, ColumnHeaderAutoResizeStyle.ColumnContent);
                UsersListView.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.ColumnContent);
                UsersListView.AutoResizeColumn(3, ColumnHeaderAutoResizeStyle.ColumnContent);
                UsersListView.AutoResizeColumn(4, ColumnHeaderAutoResizeStyle.ColumnContent);
            }
        }


        /// <summary>
        /// Szerkesztés gombra kattintás.
        /// Betölti az adatokat az input mezőkbe.
        /// </summary>
        private void EditBtn_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedUserId = UsersListView.SelectedIndices[0];
                FirstnameBox.Text = Users[SelectedUserId].first_name;
                LastnameBox.Text = Users[SelectedUserId].last_name;
                EmailBox.Text = Users[SelectedUserId].email;
                groupBox2.Visible = false;
                if (Users[SelectedUserId].category == "contractor") {
                    groupBox2.Visible = true;
                    JobNamebox.Text = Users[SelectedUserId].job_name;
                    JobTitlebox.Text = Users[SelectedUserId].job_title;
                    if (StatesComboBox.Items.Count == 0)
                    {
                        Database a = new Database(externalServer);
                        mdi = a.Connect();
                        mdi.open();
                        string sql = "SELECT state_name FROM states";
                        List<string> states = mdi.getOneFieldToList(sql);
                        for (int i = 0; i < states.Count; i++)
                        {
                            StatesComboBox.Items.Add(states[i].ToString());
                        }
                        mdi.close();
                    }
                    StatesComboBox.SelectedIndex = Users[SelectedUserId].state_id-1;
                }
                InsideTabControl.SelectTab(1);
                EditBtn.Text = "Mentés";
                EditBtn.Click -= EditBtn_Click;
                EditBtn.Click += SaveBtn_Click;
                BackBtn.Show();
            }
            catch {
                MessageBox.Show("Sajnáljuk hiba lépett fel a szerkesztés ablak megnyítása közben");
            }
            
        }

        /// <summary>
        /// Mentés gombra kattintás.
        /// Ellenőrzi az adatokat és ha helyesek akkor feltölti az adatbázisba.
        /// </summary>
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            bool valid = true;
            Validator v = new Validator();
            Contractor u = new Contractor();
            u.id = Users[SelectedUserId].id;
            u.category = Users[SelectedUserId].category;
            string First_name = FirstnameBox.Text;
            try
            {
                if (!v.ValidName(First_name))
                {
                    valid = false;
                }
                else {
                    u.first_name = First_name;
                }
            }
            catch (Exception error)
            {
                valid = false;
                errorProvider.SetError(FirstnameBox, error.Message);
            }

            string Last_name = LastnameBox.Text;
            try
            {
                if (!v.ValidName(Last_name))
                {
                    valid = false;
                }
                else
                {
                    u.last_name = Last_name;
                }
            }
            catch (Exception error)
            {
                valid = false;
                errorProvider.SetError(LastnameBox, error.Message);
            }

            string Email = EmailBox.Text;
            try
            {
                if (!v.ValidEmail(Email))
                {
                    valid = false;
                }
                else
                {
                    u.email = Email;
                }
            }
            catch(Exception error) {
                valid = false;
                errorProvider.SetError(EmailBox, error.Message);
            }
            u.job_name = JobNamebox.Text;
            u.job_title = JobTitlebox.Text;
            u.state_id = StatesComboBox.SelectedIndex+1;

            if (valid) {
                string sql = u.MakeSqlCode();
                Database a = new Database(externalServer);
                Console.WriteLine(sql);
                mdi = a.Connect();
                mdi.open();
                mdi.executeDMQuery(sql);
                mdi.close();
            }
        }

        /// <summary>
        /// Vissza gomra kattintás.
        /// </summary>
        private void BackBtn_Click(object sender, EventArgs e)
        {
            EditBtn.Text = "Szerkesztés";
            EditBtn.Click -= SaveBtn_Click;
            EditBtn.Click += EditBtn_Click;
            InsideTabControl.SelectTab(0);
            BackBtn.Hide();
        }

        /// <summary>
        /// Felhasználó törlése
        /// </summary>
        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (InsideTabControl.SelectedIndex == 0)
                {
                    SelectedUserId = UsersListView.SelectedIndices[0];
                }
                DialogResult dr = MessageBox.Show("Biztos törölni szeretné ezt a felhasználót?", "Törlés", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (dr == DialogResult.Yes)
                {
                    Database a = new Database(externalServer);
                    string sql = "DELETE FROM users WHERE userID=" + Users[SelectedUserId].id+";";
                    sql += "DELETE FROM chat WHERE sender=" + Users[SelectedUserId].id + " OR taker=" + Users[SelectedUserId].id + ";";
                    if (Users[SelectedUserId].category == "contractor")
                    {
                        sql += "DELETE FROM contractors WHERE userID=" + Users[SelectedUserId].id;
                    }

                    mdi = a.Connect();
                    mdi.open();
                    mdi.executeDMQuery(sql);
                    mdi.close();
                    if (InsideTabControl.SelectedIndex != 0)
                    {
                        BackBtn.PerformClick();
                    }
                    else
                    {
                        LoadUsersToListView();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Sajnáljuk hiba lépett fel a felhasználó törlése közben");
            }
        }
    }
}
