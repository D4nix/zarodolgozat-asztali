﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ConnectToMysqlDatabase
{
    class Database
    {
        private bool externalServer;
        private string server = "localhost";
        private string database = "szaki.hu";
        private string port = "3306";
        private string username = "root";
        private string password = "";
        public Database(bool externalServer)
        {
            this.externalServer = externalServer;
        }
        public MySQLDatabaseInterface Connect()
        {
            if (externalServer) {
                getDatabaseConfigFromfile();
            }
            MySQLDatabaseInterface mdi = new MySQLDatabaseInterface();
            mdi.setErrorToUserInterface(true);
            mdi.setErrorToGraphicalUserInterface(false);
            mdi.setConnectionServerData(server, database, port);
            mdi.setConnectionUserData(username, password);
            mdi.makeConnectionToDatabase();        

            return mdi;
        }

        private void getDatabaseConfigFromfile()
        {
            StreamReader SR = new StreamReader("databaseConfig.txt");
            SR.ReadLine();
            string[] row = SR.ReadLine().Split(';');
            server   = row[0];
            database = row[1];
            username = row[2];
            password = row[3];
            port     = row[4];
        }
    }
}
