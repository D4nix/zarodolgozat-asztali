﻿using ConnectToMysqlDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace szaki.hu.Model
{
    class Contractor : User
    {
        public Contractor()
        {
        }

        public Contractor(int id, string first_name, string last_name, string email, string category, string job_name, string job_title, string state,int state_id) : base(id, first_name, last_name, email, category)
        {
            this.job_name = job_name;
            this.job_title = job_title;
            this.state = state;
            this.state_id = state_id;
        }

        public override string MakeSqlCode() {
            if (category == "individual")
            {
                return base.MakeSqlCode();
            }
            else
            {
                string sql = base.MakeSqlCode();
                sql += String.Format(";UPDATE contractors SET job_name = '{0}', job_title='{1}', state = {2} WHERE userID = {3}", job_name, job_title, state_id, id);
                return sql;
            }
        }

        public string job_name { set; get; }
        public string job_title { set; get; }
        public string state { set; get; }
        public int state_id { set; get; }
    }
}
