﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace szaki.hu.Model
{
    class User
    {
        public int id { set; get; }
        public string first_name { set; get; }
        public string last_name { set; get; }
        public string email { set; get; }
        public string category { set; get; }

        public User(int id, string first_name, string last_name, string email, string category)
        {
            this.id = id;
            this.first_name = first_name;
            this.last_name = last_name;
            this.email = email;
            this.category = category;
        }

        public User()
        { }

        public virtual string MakeSqlCode() {
            string sql = String.Format("UPDATE users SET first_name = '{0}',last_name = '{1}',email = '{2}', category= '{3}' WHERE userID ={4}", first_name, last_name, email, category, id);
            return sql;
        }
    }
}
