﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Validation
{
    /// <summary>
    /// Ellenörző osztály
    /// </summary>
    public class Validator
    {

        /// <summary>
        /// Ellenőrzi az e-mail címet
        /// </summary>
        /// <param name="email">Ellenőrizendő e-mail cím</param>
        /// <returns>Helyes-e az e-mail cím</returns>
        /// <exception cref="Exceptions">Hibás e-mail cím esetén.</exception>
        #region EllenőrzőEmail
        public bool ValidEmail(String email)
        {
            bool ok = true;
            if (!ValidEmailEmpty())
            {
                throw new Exception("Az e-mail cím mező nem lehet üres!");
            }
            if (!ValidEmailat())
            {
                throw new Exception("Az e-mail-nek tartalmaznia kell \"@\" jelet!");
            }
            if (!ValidEmaildot())
            {
                throw new Exception("Az e-mail-nek tartalmaznia kell \".\" jelet!");
            }
            return ok;

            bool ValidEmailEmpty()
            {

                if (email.Replace(" ", "") != string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

            bool ValidEmailat()
            {
                if (email.Contains('@'))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            bool ValidEmaildot()
            {
                if (email.Contains('.'))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        /// <summary>
        /// Ellenőrzi az jelszavat
        /// </summary>
        /// <param name="password">Ellenőrizendő jelszó</param>
        /// <returns>Helyes-e az jelszó</returns>
        /// <exception cref="Exceptions">Hibás jelszó esetén.</exception>
        #region EllenőrzőJelszó
        public bool ValidPassword(String password)
        {
            bool ok = true;
            if (!ValidPasswordEmpty())
            {
                throw new Exception("A jelszó mező nem lehet üres!");
            }
            if (!ValidPasswordLowerLetter())
            {
                throw new Exception("Az jelszónak tartalmaznia kell kisbetűt!");
            }
            if (!ValidPasswordUpperLetter())
            {
                throw new Exception("Az jelszónak tartalmaznia kell nagybetűt!");
            }
            if (!ValidPasswordNumber())
            {
                throw new Exception("A jelszónak tartalmaznia kell számot!");
            }
            if (!ValidPasswordAtleast())
            {
                throw new Exception("Az jelszónak legalább 9 karakter hosszúnak kell lennie");
            }
            return ok;

            bool ValidPasswordEmpty()
            {

                if (password.Replace(" ", "")!=string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

            bool ValidPasswordLowerLetter()
            {

                if (Regex.IsMatch(password, @"^(?=.*[a-zöüóőúéáű])"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }

            bool ValidPasswordUpperLetter()
            {

                if (Regex.IsMatch(password, @"^(?=.*[A-ZÖÜÓŐÚÉÁŰ])"))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

            bool ValidPasswordNumber()
            {

                if (Regex.IsMatch(password, @"^(?=.*[0-9])"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            bool ValidPasswordAtleast()
            {

                if (password.Length >= 9)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }


        }
        #endregion

        public bool ValidName(string name) {
            
             if (Regex.IsMatch(name, @"(^[A-ZÖÜÓŐÚÉÁŰ])+[a-zöüóőúéáű]+$"))
             {
                return true;
             }
             else
             {
                throw new Exception("Az első karaternek nagybetűnek kell lennie!A többi kisbetű");
            }
        }

    }
}
