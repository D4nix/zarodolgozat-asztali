﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Tests
{
    [TestClass()]
    public class ValidatorTests
    {
        /// <summary>
        /// Teszteli az e-mail címet
        /// </summary>
        #region emailTest
        [TestMethod()]
        public void ValidEmailTestValid()
        {
            try
            {
                bool expected = true;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidEmail("gabord162@gmail.com"), "Helyes email címre hibát dob ki!");
            }
            catch (Exception e)
            {
                Assert.Fail("" + e);
            }
        }

        [TestMethod()]
        public void ValidEmailEmpty()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidEmail("   "), "Üres mezőre nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("Az e-mail cím mező nem lehet üres!"), "Rossz kivételt dob ki!");
            }
        }

        [TestMethod()]
        public void ValidEmailTestNoAt()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidEmail("gabord162gmail.com"), "Ha nem tartalmazz \"@\" karaktert akkor nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("Az e-mail-nek tartalmaznia kell \"@\" jelet!"), "Rossz kivételt dob ki!");
            }
        }

        [TestMethod()]
        public void ValidEmailTestNoDot()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidEmail("gabord162@gmailcom"), "Ha nem tartalmazz \".\" karaktert akkor nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("Az e-mail-nek tartalmaznia kell \".\" jelet!"), "Rossz kivételt dob ki!");
            }
        }
        #endregion

        /// <summary>
        /// Teszteli a jelszót
        /// </summary>
        #region passwordTest
        [TestMethod()]
        public void ValidPasswordValid()
        {
            try
            {
                bool expected = true;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidPassword("aáAÁ0123456789"), "Helyes jelszóra címre hibát dob ki!");
            }
            catch (Exception e)
            {
                Assert.Fail("" + e);
            }
        }

        [TestMethod()]
        public void ValidpasswordEmpty()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidPassword("   "), "Üres mezőre nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("A jelszó mező nem lehet üres!"), "Rossz kivételt dob ki!");
            }
        }

        [TestMethod()]
        public void ValidPasswordTestNoLowerLetter()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidPassword("A125534874"), "Ha nem tartalmazz kisbetűt akkor nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("Az jelszónak tartalmaznia kell kisbetűt!"), "Rossz kivételt dob ki!");
            }
        }

        [TestMethod()]
        public void ValidPasswordTestNoUpperLetter()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidPassword("a125538744"), "Ha nem tartalmazz nagybetűt akkor nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("Az jelszónak tartalmaznia kell nagybetűt!"), "Rossz kivételt dob ki!");
            }
        }
        [TestMethod()]
        public void ValidPasswordTestNoNumber()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidPassword("Nincsszám"), "Ha nem tartalmazz számot akkor nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("A jelszónak tartalmaznia kell számot!"), "Rossz kivételt dob ki!");
            }
        }
        [TestMethod()]
        public void ValidPasswordTestNotLongEnough()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidPassword("aA12"), "Ha a jelszó nem elég hosszú akkor nem dob hibát!");
            }
            catch (Exception e)
            {
                bool expected = true;
                string actual = "" + e;
                Assert.AreEqual(expected, actual.Contains("Az jelszónak legalább 9 karakter hosszúnak kell lennie"), "Rossz kivételt dob ki!");
            }
        }
        #endregion

        /// <summary>
        /// Teszteli az nevet
        /// </summary>
        #region NameTest
        [TestMethod()]
        public void ValidNameTestValid()
        {
            try
            {
                bool expected = true;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidName("Dániel"), "Helyes névre hibát dob ki!");
            }
            catch (Exception e)
            {
                Assert.Fail("" + e);
            }
        }

        [TestMethod()]
        public void ValidNameTestEmpty()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidName("   "), "Üres mezőre nem dob hibát!");
            }
            catch (Exception e)
            {
                
            }
        }

        [TestMethod()]
        public void ValidNameTestNotUppercase()
        {
            try
            {
                bool expected = false;
                Validator actual = new Validator();
                Assert.AreEqual(expected, actual.ValidName("daniel"), "Üres mezőre nem dob hibát!");
            }
            catch (Exception e)
            {

            }
        }
        #endregion
    }
}